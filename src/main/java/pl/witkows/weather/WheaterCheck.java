package pl.witkows.weather;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class WheaterCheck {

    static void sprawdzPogodeWMiescie (Scanner scanner) {
        System.out.println("Prosze podaj miasto:");
        String miasto = scanner.next();


//        String url = "http://api.openweathermap.org/data/2.5/weather?q=" + miasto + ",pl&APPID=1d4cbd5eae1fbb37f92be746f37e4615&units=metric";

        URL url = new HttpUrl.Builder()
                .scheme("http")
                .host("api.openweathermap.org")
                .addPathSegments("data/2.5/weather")
                .addQueryParameter("q", miasto + ",pl")
                .addQueryParameter("APPID", Api.KEY)
                .addQueryParameter("units", "metric")
                .build().url();

         String endPoint = url.toString();



        try {
            String result = run(url.toString());
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String run(String url) throws IOException {
        OkHttpClient client = OkHttp.INSTANCE.getClient();

        Request request = new Request.Builder().url(url).build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}
