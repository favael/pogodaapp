package pl.witkows.weather;

import java.util.Scanner;

import static pl.witkows.weather.WheaterCheck.sprawdzPogodeWMiescie;

public enum Menu {
    SINGLE_CHECK(0, "Sprawdź pogodę dla miasta"),
    INTERVAL_CHECK(1, "Sprawdź pogodę cyklicznie"),
    SETTINGS(2, "ustawienia"),
    EXIT(3, "Wyjście");

    private final int menuPosition;
    private final String menuLable;


    Menu (int menupositions, String label) {
        this.menuPosition = menupositions;
        this.menuLable = label;
    }

    public Menu getMenuForPosition (int menuPosition) {
        for (Menu item : Menu.values()) {
            if (item.menuPosition == menuPosition) {
                return item;
            }
        }
        return EXIT;
    }

    static void Start () {
        Scanner scanner = new Scanner(System.in, "utf-8");
        System.out.println("Witaj w aplikacji Pogodynka\n" +
                "1. Sprawdz Pogode\n" +
                "2. Wyjdz");

        switch (scanner.nextInt()) {
            case 1:
                sprawdzPogodeWMiescie(scanner);
                break;
            default:
                System.out.println("zamykam program");
                break;
        }
    }
}